package org.homework.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
public class Point {
    private double x;
    private double y;

    public double distanceTo(Point point) {
        double dx = point.x - x;
        double dy = point.y - y;
        double distanceSquare = dx * dx + dy * dy;
        return Math.sqrt(distanceSquare);
    }
}
