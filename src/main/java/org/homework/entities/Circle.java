package org.homework.entities;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Circle {
    private Point center;
    private double radius;

    public boolean containsPoint(Point point) {
        double distance = center.distanceTo(point);
        return distance < radius;
    }
}
