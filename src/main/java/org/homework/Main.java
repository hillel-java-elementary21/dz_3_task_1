package org.homework;

import org.homework.services.CircleService;
import org.homework.services.InMemoryCircleService;
import org.homework.services.InMemoryPointService;
import org.homework.services.PointService;
import org.homework.ui.menu.Menu;
import org.homework.ui.menu.MenuItems;
import org.homework.ui.menu.items.*;
import org.homework.ui.views.CircleView;
import org.homework.ui.views.ErrorView;
import org.homework.ui.views.PointView;
import org.homework.ui.views.PointListView;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("*** Program ***");
        PointService pointService = new InMemoryPointService();
        ErrorView errorView = new ErrorView();
        CircleService circleService = new InMemoryCircleService();
        PointView pointView = new PointView(scanner);
        CircleView circleView = new CircleView(scanner);
        PointListView pointListView = new PointListView(scanner, pointView);
        Menu menu = new Menu(scanner, new MenuItems[]{
                new AddPointMenuItem(errorView, pointView, pointService),
                new AddCircleMenuItem(errorView, pointService, circleService, pointView, circleView),
                new ChangeCircleMenuItem(errorView, pointService, circleService, pointView, pointListView, circleView),
                new ShowPointsMenuItem(pointService, pointListView),
                new ShowPointsInCircleMenuItem(errorView, pointListView, pointService, circleService),
                new ShowPointsOutsideCircleMenuItem(errorView, pointListView, pointService, circleService),
                new ExitMenuItem()
        });
        menu.run();
    }
}

