package org.homework.services;

import org.homework.entities.Circle;

public class InMemoryCircleService implements CircleService {
    private Circle circle;

    public Circle getCircle() {
        return this.circle;
    }

    @Override
    public void addCircle(Circle circle) {
        this.circle = circle;
    }
}

