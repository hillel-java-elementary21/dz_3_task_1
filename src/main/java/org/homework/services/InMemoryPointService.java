package org.homework.services;

import org.homework.entities.Circle;
import org.homework.entities.Point;
import org.homework.utils.PointList;

public class InMemoryPointService implements PointService {
    private PointList pointList = new PointList(1);

    @Override
    public void addPoint(Point point) {
        pointList.push(point);
    }

    @Override
    public PointList getAllPoints() {
        return pointList;
    }

    @Override
    public PointList getPointsInCircle(Circle circle) {
        PointList pointsInCircle = new PointList(1);
        for (int i = 0; i < pointList.getSize(); i++) {
            if (circle.containsPoint(pointList.get(i))) {
                pointsInCircle.push(pointList.get(i));
            }
        }
        return pointsInCircle;
    }

    @Override
    public PointList getPointsOutsideCircle(Circle circle) {
        PointList pointsOutsideCircle = new PointList(1);
        for (int i = 0; i < pointList.getSize(); i++) {
            if (!(circle.containsPoint(pointList.get(i)))) {
                pointsOutsideCircle.push(pointList.get(i));
            }
        }
        return pointsOutsideCircle;
    }
}
