package org.homework.services;

import org.homework.entities.Circle;

public interface CircleService {

    void addCircle(Circle circle);

    Circle getCircle();
}

