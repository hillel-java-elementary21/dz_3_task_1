package org.homework.services;

import org.homework.entities.Circle;
import org.homework.entities.Point;
import org.homework.utils.PointList;

public interface PointService {

    void addPoint(Point point);

    PointList getAllPoints();

    PointList getPointsInCircle(Circle circle);

    PointList getPointsOutsideCircle(Circle circle);

}
