package org.homework.utils;

import lombok.Getter;
import org.homework.entities.Point;

public class PointList {
    private Point[] points;
    private int capacity;
    @Getter
    private int size;

    public PointList(int capacity) {
        this.capacity = capacity;
        this.size = 0;
        this.points = new Point[capacity];
    }

    public void push(Point point) {
        checkCapacity();
        points[size++] = point;
    }

    public Point get(int i) {
        return points[i];
    }

    private void checkCapacity() {
        if (size + 1 > capacity) {
            capacity *= 2;
            reorganisePoints();
        }
    }

    private void reorganisePoints() {
        Point[] temp = new Point[capacity];
        for (int i = 0; i < points.length; i++) {
            temp[i] = points[i];
        }
        points = temp;
    }
}
