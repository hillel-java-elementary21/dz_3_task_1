package org.homework.ui.views;

import lombok.AllArgsConstructor;
import org.homework.entities.Point;
import org.homework.utils.PointList;

import java.util.Scanner;

@AllArgsConstructor
public class PointListView {
    private Scanner scanner;
    PointView pointView;

    public void showAll(PointList pointList) {
        System.out.println("********All points**********");
        for (int i = 0; i < pointList.getSize(); i++) {
            pointView.show(pointList.get(i));
        }
        System.out.println("****************************");
    }

    public void showInCircle(PointList pointsInCircle) {
        System.out.println("***Points in a circle***");
        for (int i = 0; i < pointsInCircle.getSize(); i++) {
            pointView.show(pointsInCircle.get(i));
        }
        System.out.println("************************");
    }

    public void showOutsideCircle(PointList pointsInCircle) {
        System.out.println("***Points outside the circle ***");
        for (int i = 0; i < pointsInCircle.getSize(); i++) {
            pointView.show(pointsInCircle.get(i));
        }
        System.out.println("********************************");
    }
}
