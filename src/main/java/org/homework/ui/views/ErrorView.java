package org.homework.ui.views;

public class ErrorView {
    public void showError(String message) {
        System.out.println("x".repeat(50));
        System.out.printf("|%48s|\n", message);
        System.out.println("x".repeat(50));
    }
}
