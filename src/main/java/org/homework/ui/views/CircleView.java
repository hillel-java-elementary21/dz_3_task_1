package org.homework.ui.views;

import lombok.AllArgsConstructor;

import java.util.Scanner;

@AllArgsConstructor
public class CircleView {
    private Scanner scanner;

    public Double readRadius() {
        System.out.print("Enter the radius: ");
        double radius = scanner.nextDouble();
        scanner.nextLine();
        return radius;
    }
}
