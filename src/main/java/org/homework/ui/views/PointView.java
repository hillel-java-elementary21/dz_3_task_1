package org.homework.ui.views;

import lombok.AllArgsConstructor;
import org.homework.entities.Point;

import java.util.Scanner;

@AllArgsConstructor
public class PointView {
    private Scanner scanner;

    public void show(Point point) {
        System.out.println("-------Point-------");
        System.out.printf("x: %.2f ", point.getX());
        System.out.printf("y: %.2f\n", point.getY());
    }

    public Point readPoint() {
        System.out.print("Enter the x-axis coordinate: ");
        double x = scanner.nextDouble();
        scanner.nextLine();
        System.out.print("Enter the y-axis coordinate: ");
        double y = scanner.nextDouble();
        scanner.nextLine();
        return new Point(x, y);
    }
}
