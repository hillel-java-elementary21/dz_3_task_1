package org.homework.ui.menu;

public interface MenuItems {

    String getName();

    void execute();

    default boolean ifFinal() {
        return false;
    }
}
