package org.homework.ui.menu.items;

import lombok.AllArgsConstructor;
import org.homework.services.CircleService;
import org.homework.services.PointService;
import org.homework.ui.menu.MenuItems;
import org.homework.ui.views.ErrorView;
import org.homework.ui.views.PointListView;

@AllArgsConstructor
public class ShowPointsOutsideCircleMenuItem implements MenuItems {
    ErrorView errorView;
    PointListView pointListView;
    PointService pointService;
    CircleService circleService;

    @Override
    public String getName() {
        return "Show points outside the circle ";
    }

    @Override
    public void execute() {
        if (circleService.getCircle() != null) {
            pointListView.showOutsideCircle(pointService.getPointsOutsideCircle(circleService.getCircle()));
        } else {
            errorView.showError("Error! No circle specified.");
        }
    }
}
