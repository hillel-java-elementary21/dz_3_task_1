package org.homework.ui.menu.items;

import org.homework.ui.menu.MenuItems;

public class ExitMenuItem implements MenuItems {
    @Override
    public String getName() {
        return "EXIT";
    }

    @Override
    public void execute() {
        System.out.println("The program has completed its work. See you soon... ");
    }

    @Override
    public boolean ifFinal() {
        return true;
    }
}
