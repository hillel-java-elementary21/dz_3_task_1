package org.homework.ui.menu.items;

import lombok.AllArgsConstructor;
import org.homework.services.PointService;
import org.homework.ui.menu.MenuItems;
import org.homework.ui.views.PointListView;

@AllArgsConstructor
public class ShowPointsMenuItem implements MenuItems {
    PointService pointService;
    PointListView pointListView;

    @Override
    public String getName() {
        return "Show all points";
    }

    @Override
    public void execute() {
        pointListView.showAll(pointService.getAllPoints());
    }
}
