package org.homework.ui.menu.items;

import lombok.AllArgsConstructor;
import org.homework.entities.Point;
import org.homework.services.PointService;
import org.homework.ui.menu.MenuItems;
import org.homework.ui.views.ErrorView;
import org.homework.ui.views.PointView;

@AllArgsConstructor
public class AddPointMenuItem implements MenuItems {
    ErrorView errorView;
    PointView pointView;
    PointService pointService;

    @Override
    public String getName() {
        return "Add point";
    }

    @Override
    public void execute() {
        Point point = pointView.readPoint();
        pointService.addPoint(point);
    }
}
