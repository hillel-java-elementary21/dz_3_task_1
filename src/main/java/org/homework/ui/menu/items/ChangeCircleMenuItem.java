package org.homework.ui.menu.items;

import lombok.AllArgsConstructor;
import org.homework.entities.Circle;
import org.homework.entities.Point;
import org.homework.services.CircleService;
import org.homework.services.PointService;
import org.homework.ui.menu.MenuItems;
import org.homework.ui.views.CircleView;
import org.homework.ui.views.ErrorView;
import org.homework.ui.views.PointListView;
import org.homework.ui.views.PointView;

@AllArgsConstructor
public class ChangeCircleMenuItem implements MenuItems {
    ErrorView errorView;
    PointService pointService;
    CircleService circleService;
    PointView pointView;
    PointListView pointListView;
    CircleView circleView;


    @Override
    public String getName() {
        return "Change circle";
    }

    @Override
    public void execute() {
        Point point = pointView.readPoint();
        double radius = circleView.readRadius();
        if (radius <= 0) {
            errorView.showError("Invalid radius entered. No data added!");
            return;
        }
        Circle circle = new Circle(point, radius);
        circleService.addCircle(circle);
    }
}
