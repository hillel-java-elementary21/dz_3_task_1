package org.homework.ui.menu;

import lombok.AllArgsConstructor;

import java.util.Scanner;

@AllArgsConstructor
public class Menu {
    Scanner scanner;
    MenuItems[] items;

    public void run() {
        while (true) {
            showMenu();
            int choice = getUserChoice();
            if (choice < 0 || choice >= items.length) {
                System.out.println("The answer is wrong! Re-enter");
                continue;
            }
            items[choice].execute();
            if (items[choice].ifFinal()) break;
        }
    }

    private int getUserChoice() {
        System.out.print("Enter your choice: ");
        if (scanner.hasNextInt()) {
            int ch = scanner.nextInt();
            scanner.nextLine();
            return ch - 1;
        } else {
            scanner.nextLine();
            return items.length + 1;
        }
    }

    private void showMenu() {
        System.out.println("----------------Menu-----------------");
        for (int i = 0; i < items.length; i++) {
            System.out.printf("%2d - %s\n", i + 1, items[i].getName());
        }
        System.out.println("-------------------------------------");
    }
}
