package org.homework.entities;

import org.homework.utils.PointList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PointListTest {
    @Test
    public void testPush() {
        Point p1 = new Point(1,2);
        Point p2 = new Point(3,4);
        Point p3 = new Point(-5,6);
        Point p4 = new Point(7,0);
        Point p5 = new Point(0,10);
        Point p6 = new Point(0,0);
        Point p7 = new Point(-15,-60);
        Point p8 = new Point(0,-60);
        Point p9 = new Point(-15,0);
        Point p10 = new Point(150,-60);
        PointList pointLists1 = new PointList(1);
        PointList pointLists2 = new PointList(7);
        pointLists1.push(p1);
        pointLists1.push(p2);
        pointLists1.push(p3);
        pointLists1.push(p4);
        pointLists1.push(p5);
        pointLists1.push(p6);
        pointLists1.push(p7);
        pointLists2.push(p1);
        pointLists2.push(p2);
        pointLists2.push(p3);
        pointLists2.push(p4);
        pointLists2.push(p5);
        pointLists2.push(p6);
        pointLists2.push(p7);
        pointLists2.push(p8);
        pointLists2.push(p9);
        pointLists2.push(p10);

        assertAll(
                ()->assertEquals(p1,pointLists1.get(0)),
                ()->assertEquals(p2,pointLists1.get(1)),
                ()->assertEquals(p3,pointLists1.get(2)),
                ()->assertEquals(p4,pointLists1.get(3)),
                ()->assertEquals(p5,pointLists1.get(4)),
                ()->assertEquals(p6,pointLists1.get(5)),
                ()->assertEquals(p7,pointLists1.get(6)),
                ()->assertEquals(p1,pointLists2.get(0)),
                ()->assertEquals(p2,pointLists2.get(1)),
                ()->assertEquals(p3,pointLists2.get(2)),
                ()->assertEquals(p4,pointLists2.get(3)),
                ()->assertEquals(p5,pointLists2.get(4)),
                ()->assertEquals(p6,pointLists2.get(5)),
                ()->assertEquals(p7,pointLists2.get(6)),
                ()->assertEquals(p8,pointLists2.get(7)),
                ()->assertEquals(p9,pointLists2.get(8)),
                ()->assertEquals(p10,pointLists2.get(9))
        );
    }
}
