package org.homework.entities;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {
    @Test
    public void testDistanceTo() {
        Point p1 = new Point(0,0);
        Point p2 = new Point(7,0);
        Point p3 = new Point(0,7);
        Point p4 = new Point(1,1);
        Point p5 = new Point(9,7);
        Point p6 = new Point(-0.25,1.5);
        Point p7 = new Point(7.75,7.5);
        assertAll(
                ()->assertEquals(0, p1.distanceTo(p1)),
                ()->assertEquals(7, p1.distanceTo(p2)),
                ()->assertEquals(7, p1.distanceTo(p3)),
                ()->assertEquals(10, p5.distanceTo(p4)),
                ()->assertEquals(10, p6.distanceTo(p7))
        );
    }
}