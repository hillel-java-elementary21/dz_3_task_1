package org.homework.entities;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CircleTest {
    @Test
    public void testContainsPoint() {
        Point p1 = new Point(0,0);
        Point p2 = new Point(17,0);
        Point p3 = new Point(9.99,0);
        Point p4 = new Point(0,10.01);
        Point p5 = new Point(9,1);
        Point p6 = new Point(0.1,17.5);
        Point p7 = new Point(5,5);
        Point p8 = new Point(10,15);
        Point p9 = new Point(5,-4.99);
        Circle c1 = new Circle(p1,10);
        Circle c2 = new Circle(p2, 17.5);
        Circle c3 = new Circle(p7,10);

        assertAll(
                ()->assertTrue(c1.containsPoint(p1)),
                ()->assertTrue(c1.containsPoint(p3)),
                ()->assertTrue(c1.containsPoint(p5)),
                ()->assertTrue(c2.containsPoint(p2)),
                ()->assertTrue(c3.containsPoint(p9)),
                ()->assertFalse(c1.containsPoint(p2)),
                ()->assertFalse(c1.containsPoint(p4)),
                ()->assertFalse(c1.containsPoint(p6)),
                ()->assertFalse(c3.containsPoint(p8)),
                ()->assertFalse(c3.containsPoint(p2))
        );
    }
}
